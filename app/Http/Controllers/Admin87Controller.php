<?php

namespace App\Http\Controllers;

use App\Models\Agama87;
use App\Models\User;
use Illuminate\Http\Request;

class Admin87Controller extends Controller
{
    public function admindashboardpaged87()
    {
        $user = User::where('role', 'user')->get();
        $agama = Agama87::all();

        return view('dashboard', ['data' => $user, 'agama' => $agama]);
    }

    public function adminagamapage87()
    {
        $agama = Agama87::all();

        return view('agama', ['all_agama' => $agama]);
    }

    public function editAdminAgamaPage87(Request $request)
    {

        $id = $request->id;

        $agama = Agama87::find($id);

        if (!$agama) {
            return back()->with('error', 'Agama tidak ditemukan');
        }

        $all_agama = Agama87::all();

        return view('agama', ['all_agama' => $all_agama, 'agama' => $agama]);
    }

    public function detailAdminPage87(Request $request)
    {
        $id = $request->id;
        $user = User::find($id);

        if (!$user) {
            return redirect('/dashboard87')->with('error', 'User tidak ditemukan');
        }

        $agama = Agama87::all();

        $detail = $user->detail;
        $data = array_merge($user->toArray(), $detail->toArray());

        return view('profile', ['user' => $data, 'agama' => $agama, 'is_preview' => true]);
    }


    public function updateStatusUser87(Request $request)
    {
        $id = $request->id;
        $user = User::find($id);

        if (!$user) {
            return redirect('/dashboard87')->with('error', 'User tidak ditemukan');
        }

        $updateStatus = $user->update([
            'is_active' => $user->is_active == 1 ? 0 : 1
        ]);

        if ($updateStatus) {
            return redirect('/dashboard87')->with('success', 'Status berhasil diubah');
        } else {
            return redirect('/dashboard87')->with('error', 'Status gagal diubah');
        }
    }

    public function updateAdminAgamaUser87(Request $request)
    {
        $id = $request->id;
        $user = User::find($id);

        if (!$user) {
            return redirect('/dashboard87')->with('error', 'User tidak ditemukan');
        }

        $request->validate([
            'agama' => 'required|exists:agama87,id'
        ]);

        $user->detail->id_agama = $request->agama;
        $updateAgama = $user->detail->save();

        if ($updateAgama) {
            return redirect('/dashboard87')->with('success', 'Agama berhasil diubah');
        } else {
            return redirect('/dashboard87')->with('error', 'Agama gagal diubah');
        }
    }


    public function hapusAdminAgama87(Request $request)
    {
        $id = $request->id;
        $agama = Agama87::find($id);

        if (!$agama) {
            return redirect('/agama87')->with('error', 'Agama tidak ditemukan');
        }

        $deleteAgama = $agama->delete();


        if ($deleteAgama) {
            return redirect('/agama87')->with('success', 'Agama berhasil dihapus');
        } else {
            return redirect('/agama87')->with('error', 'Agama gagal dihapus');
        }
    }

    public function ubahAdminAgama87(Request $request)
    {
        $id = $request->id;
        $agama = Agama87::find($id);

        if (!$agama) {
            return redirect('/agama87')->with('error', 'Agama tidak ditemukan');
        }

        $request->validate([
            'nama_agama' => 'required'
        ]);

        $updateAgama = $agama->update([
            'nama_agama' => $request->nama_agama
        ]);

        if ($updateAgama) {
            return redirect('/agama87')->with('success', 'Agama berhasil diubah');
        } else {
            return redirect('/agama87')->with('error', 'Agama gagal diubah');
        }
    }


    public function admincreateAdmin87(Request $request)
    {
        $request->validate([
            'nama_agama' => 'required'
        ]);

        $createAgama = Agama87::create([
            'nama_agama' => $request->nama_agama
        ]);

        if ($createAgama) {
            return redirect('/agama87')->with('success', 'Agama berhasil ditambahkan');
        } else {
            return redirect('/agama87')->with('error', 'Agama gagal ditambahkan');
        }
    }
}
