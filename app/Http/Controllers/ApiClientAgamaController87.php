<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use App\Models\Agama87;
use App\Models\user87;

class ApiClientAgamaController87 extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index87()
    {
        $client = new Client();
        $response = $client->request('GET','http://127.0.0.1/UAS_Backend/public/api/agama87/listagama87');
        $statusCode = $response->getStatusCode();
        $body = $response->getBody();
        $data = json_decode($body,true);

        //dd($data);
        return view('/apiclienttic/tampilanagama87',['agama'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create87()
    {
        $client = new Client();
        $response = $client->request('GET','http://127.0.0.1/UAS_Backend/public/api/agama87/listagama87');
        $statusCode = $response->getStatusCode();
        $body = $response->getBody();
        $data = json_decode($body,true);
        dd($data);
        return view('/apiclienttic/listagama87', ['agama'=>$data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store87(Request $request)
    {
        $client = new Client();
        $response = $client->request('POST','http://127.0.0.1/UAS_Backend/public/api/agama87/tambahagama87',
        [
            'json' => [
                'nama_agama' => $request->nama_agama,
            ]
        ]
        );

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show87($id)
    {
        $client = new Client ();
        $response = $client->request('GET','http://127.0.0.1/UAS_Backend/public/api/agama87/detailtagama87/'.$id,);
        // $Users = Auth::user()->nama;
        $statusCode = $response->getStatusCode();
        $body = $response->getBody();

        $data = json_decode($body,true);

        return view('/apiclienttic/editagama',['agama'=>$data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update87(Request $request, $id)
    {
        $client = new Client();
        $response = $client->request('PUT','http://127.0.0.1/UAS_Backend/public/api/agama87/update87/' .$id, 
        [
            'json' => [
                'nama_agama' => $request->nama_agama,
            ]
        ]
        );

        return redirect("/agama87/clientapi/listagama87");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy87($id)
    {
        $client = new Client();
        $response = $client->request('DELETE','http://127.0.0.1/UAS_Backend/public/api/agama87/' .$id, 
        );

    return back();
    }
}
