<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\FormatApi;
use App\Models\Agama87;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AgamaApiController87 extends Controller
{
    public function index87(Request $request)
    {
        $agama =Agama87::all();

        return new FormatApi(true, 'Berhasil Mendapatkan data agama', $agama);
    }

    public function store87(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_agama' => 'required',
        ]);

        if($validator->fails())
        {
            return new FormatApi(false, 'Validasi gagal', $validator->errors()->all());
        }

        $agama = Agama87::create($request->all());

        if(!$agama)
        {
            return new FormatApi(false, 'Agama tidak ditemukan', null);
        }

        return new FormatApi(true, 'Berhasil menambah data agama', null);
    }

    public function show87($id)
    {
        $agama = Agama87::find($id);

        if(!$agama)
        {
            return new FormatApi(false, 'Agama tidak ditemukan', null);
        }

        return new FormatApi(true, 'Berhasil mendapatkan data agama', $agama);
    }

    public function update87 (Request $request, $id)
    {
        $validator = Validator::make($request->all (), [
            'nama_agama' => 'required',
        ]);

        if($validator->fails())
        {
            return new FormatApi(false, 'Validasi gagal', $validator->errors()->all());
        }

        $agama = Agama87::find($id);

        $agama->update ([
            'nama_agama' => $request->nama_agama,
        ]);
        if(!$agama)
        {
            return new FormatApi(false, 'Agama tidak ditemukan', null);
        }

        return new FormatApi(true, 'Berhasil menguabah data', null);
    }

    public function destroy87 (Request $request, $id)
    {
        $agama = Agama87::find($id);

        if(!$agama) {
            return new FormatApi(false, "Agama tidak ditemukan", null);
        }

        $agama->delete();
            return new FormatApi(true, "Berhasil menghapus agama", null);
        
    }

    // public function getAgamma87(Request $request)
    // {
    //     $agama = Agama87::all();

    //     return new FormatApi(true, 'Berhasil mendapatkan data agama', $agama);
    // }

    // public function postAgama87(Request $request, $id)
    // {
    //     $agama = Agama87::find($id);

    //     if(!$agama) {
    //         return new FormatApi(false, 'Agama not found', null);
    //     }

    //     $validator = Validator::make($request->all(), [
    //         'nama_agama' => 'required',
    //     ]);

    //     if ($validator->fails()) {
    //         return new FormatApi(false, 'Validasi Gagal', $validator->errors()->all());
    //     }

    //     $agama->update([
    //         'nama_agama'=> $request->nama_agama,
    //     ]);

    //     return new FormatApi(true, 'Berhasil Menambah Data Agama', null);
    // }

    // public function putAgama87(Request $request, $id)
    // {
    //     $agama = Agama87::find($id);

    //     if (!$agama) {
    //         return new FormatApi(false, 'Agama Tidak DItemukan', null);
    //     }

    //     $validator = Validator::make($request->all(), [
    //         'nama_agama' => 'required',
    //     ]);

    //     if ($validator->fails()) {
    //         return new FormatApi(false, 'Validasi Gagal', $validator->errors()->all());
    //     }

    //     $agama->update([
    //         'nama_agama'=> $request->nama_agama,
    //     ]);

    //     return new FormatApi(true, 'Berhasil Mengubah Data Agama', null);
    // }

    // public function deleteAgama87(Request $request, $id)
    // {
    //     $agama = Agama87::find($id);

    //     if (!$agama) {
    //         return new FormatApi(false, 'Agama Tidak DItemukan', null);
    //     }

    //     $agama->delete();
    //     return new FormatApi(true, 'Berhasil Menghapus Data Agama', null);
    // }

}
