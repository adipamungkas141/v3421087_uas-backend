<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Agama87;
use App\Models\user87;
use Illuminate\Support\Facades\Auth;


class ApiClientUsers87 extends Controller
{
    //Dashboard Admin   
    public function index87()
    {
        $client = new Client ();
        $response = $client ->request('GET', 'http://127.0.0.1/UAS_Backend/public/api/users87');
        $responseAgama = $client ->request('GET', 'http://127.0.0.1/UAS_Backend/public/api/agama87/listagama87');

        // $agama = Agama87::all();
        $statusCode = $response->getStatusCode();
        $body = $response->getBody();
        $bodyAgama = json_decode($responseAgama->getBody(), true);

        $data = json_decode($body,true);

        //dd($data);
        return view('/apiclienttic/users87', ['user'=>$data['data'], 'agama'=>$bodyAgama['data'], 'use_client' => true]);
    }

    //Detail User
public function show87($id)
    {
        $client = new Client ();
        $response = $client ->request('GET', 'http://127.0.0.1/UAS_Backend/public/api/users87/'.$id.'/detail');
        $agama = Agama87::all();
        $statusCode = $response->getStatusCode();
        $body = $response->getBody();

        $data = json_decode($body,true);
        return view('/apiclienttic/detailuser', ['user'=>$data['data'], 'is_preview' => true, 'agama'=>$agama]);
    }


public function destroy87($id)
    {
        $client = new Client ();
        $response = $client ->request('DELETE', 'http://127.0.0.1/UAS_Backend/public/api/users87/'.$id);
        $statusCode = $response->getStatusCode();
        $body = $response->getBody();

        $data = json_decode($body,true);
        return redirect('/user/clientapi/userlist');
    }
public function status87($id)
{
    $client = new Client ();
    $response = $client ->request('PUT', 'http://127.0.0.1/UAS_Backend/public/api/users87/status87/'.$id);
    $statusCode = $response->getStatusCode();
        $body = $response->getBody();

        $data = json_decode($body,true);
        return redirect('/user/clientapi/userlist');
}

public function update87(Request $request, $id)
{
    $client = new Client ();
    $response = $client ->request('PUT', 'http://127.0.0.1/UAS_Backend/public/api/detail87/update/' .$id, 
    [
        'json' => [
            'id_agama' => $request->id_agama,
        ]
    ]
    );
    
    $statusCode = $response->getStatusCode();
        $body = $response->getBody();

        $data = json_decode($body,true);

        
        return back();
}
}