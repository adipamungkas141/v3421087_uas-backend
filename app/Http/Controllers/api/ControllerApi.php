<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\FormatApi;
use App\Models\Agama87;
use Illuminate\Http\Request;

class ControllerApi extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataagama = Agama87::latest()->get();
        return new FormatApi(true, 'Detail Data', $dataagama);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $addagama = Agama87::create([
                'nama_agama'=>$request->nama_agama
        ]);
        return new FormatApi(true, 'Tambah Data', $addagama);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataagama = Agama87::where('id', '=', $id)->get();
        return new FormatApi(true, 'Detail Agama', $dataagama);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agama = Agama87::findOrFall($id)->get();
        $agama->update([
            'nama_agama' => $request->nama_agama,
        ]);
        return new FormatApi(true, 'Update Data Agama', $agama);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $agama = Agama87::findOrFail($id);
        $agama ->delete();

        return new FormatApi(true, 'Hapus Data Agama', $agama);
    }
}
