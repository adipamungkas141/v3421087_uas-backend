<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\FormatApi;
use App\Models\Agama87;
use Database\Seeders\Agama;
use App\Http\Controllers\api\AgamaApiController87;

class ControllerApiAgama87 extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index87()
    {
        $dataagama = Agama87::latest()->get();
        return new FormatApi(true, 'Detail Data', $dataagama);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create87()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store87(Request $request)
    {
        $addAgama = Agama87::create([
            'nama_agama'=>$request->nama_agama
        ]);
        return new FormatApi(true, 'Data Berhasil ditambahkan', $addAgama);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show87($id)
    {
        $dataagama = Agama87::where('id', '=', $id)->get();
        return new FormatApi(true, 'Detail Data', $dataagama); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit87($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update87(Request $request, $id)
    {
        $agama =Agama87::findOrFail($id);
        $agama->update ([
            'nama_agama' => $request->nama_agama,
        ]);

        return new FormatApi(true, 'Update Berhasil', $agama);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy87($id)
    {
        $agama = Agama87::findOrFail($id);
        $agama->delete();
        return new FormatApi(true, 'Data Berhasil dihapus', $agama);
    }
}
