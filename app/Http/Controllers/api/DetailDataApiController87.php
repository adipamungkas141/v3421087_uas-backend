<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\FormatApi;
use App\Models\Detail_data87;
use App\Models\User;

class DetailDataApiController87 extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index87()
    {
        $data = Detail_data87::with('user')->get();
        return new FormatApi(true, 'Berhasil mendapatkan data user', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update87(Request $request, $id)
    {
        $user = User::find($id);

        if (!$user) {
            return new FormatApi(false, "User tidak ditemukan", null);
        }


        $user->detail->id_agama = $request->id_agama;
        $updateAgama = $user->detail->save();

        if ($updateAgama) {
            return new FormatApi(true, "Agama user berhasil diubah", null);
        } else {
            return new FormatApi(false, "Agama user tidak dirubah", null);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
