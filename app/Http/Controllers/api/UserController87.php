<?php

namespace App\Http\Controllers\api;

use App\Http\Resources\FormatApi ;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class UserController87 extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getUsers87()
    {
        $user = User::with('detail')->where('role', 'user')->get();

        return new FormatApi(true, 'Berhasil mendapatkan data user', $user);
    }

    public function putPhotoUsers87(Request  $request, $id)
    {
        $user = User::find($id)->with('detail');

        if(!$user) {
            return new FormatApi(false, 'user not found', null);
        }

        $validator = Validator::make($request->all(), [
            'photoProfil' => 'required|image|mines:jpg,jpeg,png,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return new FormatApi(false, 'Validasi Gagal', $validator->errors()->all());
        }

        $imageName = time() .','. $request->photo->extension();
        $request->photo->move(public_path('images'), $imageName);

        $updatePhoto = $user->detail->update([
            'foto' => $imageName,
        ]);

        if ($updatePhoto) {
            return new FormatApi(true, 'Foto User Berhasil Di Ubah', null);
        } else {
            return new FormatApi(false, 'Foto User Gagal Di Ubah', null);
        }
    }

    public function putPhotoKTPP87(Request  $request, $id)
    {
        $user = User::find($id)->with('detail');

        if(!$user) {
            return new FormatApi(false, 'user not found', null);
        }

        $validator = Validator::make($request->all(), [
            'photoProfil' => 'required|image|mines:jpg,jpeg,png,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return new FormatApi(false, 'Validasi Gagal', $validator->errors()->all());
        }

        $imageName = time() .','. $request->photo->extension();
        $request->photo->move(public_path('images'), $imageName);

        $updatePhoto = $user->detail->update([
            'foto' => $imageName,
        ]);

        if ($updatePhoto) {
            return new FormatApi(true, 'Foto KTP Berhasil Di Ubah', null);
        } else {
            return new FormatApi(false, 'Foto KTP Gagal Di Ubah', null);
        }
    }

    public function putUserPassword87(Request  $request, $id)
    {
        $user = User::find($id)->with('detail');

        if(!$user) {
            return new FormatApi(false, 'user not found', null);
        }

        $validator = Validator::make($request->all(), [
            'old_password' => 'required|min8',
            'password' => 'required|min8',
            'password_confirmation' => 'required|same:password',
        ]);
    }


    public function getDetailUser87(Request  $request, $id)
    {
        $user = User::find($id)->with('detail');

        if(!$user) {
            return redirect(('/dashboard87'))->with('error', 'user not found');
        }

        $detail = $user->detail;
        $data = array_merge($user->toArray(), $detail->toArray());

        return new FormatApi(true, 'Berhasil mendapatkan data user', $data);
    }

    public function putDetailuser87(Request $request, $id)
    {
        $user = User::find($id)->with('detail');

        if(!$user) {
            return redirect(('/dashboard87'))->with('error', 'user not found');
        }
    }

    public function putuserStatus87(Request $request, $id)
    {
        $user = User::with('detail')->find($id);
        
        if(!$user) {
            return new FormatApi(false, 'User Tidak Ditemukan', null);
        }

        $validator = Validator::make($request->all(), [
            'is_active' => 'required|boolean',
        ]);

        if($validator->fails) {
            return new FormatApi(false, 'Validasi Gagal', $validator->errors()->all());
        }

        $updateStatus = $user->update([
            'is_active' => $request->is_active,
        ]);

        if ($updateStatus) {
            return new FormatApi(true, 'Berhasil Mengubah Status USer', null);
        } else {
            return new FormatApi(false, 'Gagal Mengubah Status User', null);
        }
    }
}
