<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\FormatApi;
use App\Models\Agama87;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class UserProfilController87 extends Controller
{
    //

    public function index87()
    {
        $client = new Client ();
        $Users = Auth::user()->name;
        $response = $client ->request('GET', 'http://127.0.0.1/UAS_Backend/public/api/users87/'.Auth::user()->id.'/detail');
        $agama = Agama87::all();
        $statusCode = $response->getStatusCode();
        $body = $response->getBody();

        $data = json_decode($body,true);
        return view('/apiclienttic/userprofile', ['user'=>$data['data'], 'name'=>$Users, 'is_preview' => true, 'agama'=>$agama]);
    }

    public function edit87()
    {
        $client = new Client ();
        $Users = Auth::user()->name;
        $response = $client ->request('GET', 'http://127.0.0.1/UAS_Backend/public/api/users87/'.Auth::user()->id.'/detail');
        $agama = Agama87::all();
        $statusCode = $response->getStatusCode();
        $body = $response->getBody();

        $data = json_decode($body,true);
        return view('/apiclienttic/editprofil87', ['user'=>$data['data'], 'name'=>$Users, 'is_preview' => false, 'agama'=>$agama]);
    }

    public function update87()
    {
        $client = new Client ();
        $Users = Auth::user()->name;
        $response = $client ->request('PUT', 'http://127.0.0.1/UAS_Backend/public/api/users87/'.Auth::user()->id.'/detail');
        $agama = Agama87::all();
        $statusCode = $response->getStatusCode();
        $body = $response->getBody();

        $data = json_decode($body,true);
        return view('/apiclienttic/EditProfil', ['user'=>$data['data'], 'name'=>$Users, 'is_preview' => true, 'agama'=>$agama]);
    }

    public function uploadktp87(Request $request)
    {
        $file = request('photoKTP');
        $file_path = $file->getPathname();
        $file_mime = $file->getMimeType('image');
        $file_uploaded_name = $file->getClientOriginalName();
        $client = new Client ();
        $Users = Auth::user();
        $response = $client ->request('POST', 'http://127.0.0.1/UAS_Backend/public/api/uploadApiPhotoKTP87/'.$Users->id, [
            'multipart' => [
                [
                    'name' => 'foto_ktp',
                    'contents' => fopen($file_path, 'r'),
                    'filename' => $file_uploaded_name,
                    'mime-type' => $file_mime
                ]
            ]
        ]);
        return redirect('/clientapi/EditProfile87'); 
    }

    public function uploadphoto87 (Request $request)
    {
        $file = request('photoProfil');
        $file_path = $file->getPathname();
        $file_mime = $file->getMimeType('image');
        $file_uploaded_name = $file->getClientOriginalName();
        $client = new Client ();
        $Users = Auth::user();
        $response = $client ->request('POST', 'http://127.0.0.1/UAS_Backend/public/api/uploadApiPhotoProfil87/'.$Users->id, [
            'multipart' => [
                [
                    'name' => 'foto',
                    'contents' => fopen($file_path, 'r'),
                    'filename' => $file_uploaded_name,
                    'mime-type' => $file_mime
                ]
            ]
        ]);
        return redirect('/clientapi/EditProfile87');
    }

    public function updateprofil87(Request $request)
    {
        $client = new Client ();
        $Users = Auth::user();
        $response = $client ->request('PUT', 'http://127.0.0.1/UAS_Backend/public/api/uploadApiChangeProfil/'.Auth::user()->id.'/detail');
        $ubahprofi = User::all();
        $statusCode = $response->getStatusCode();
        $body = $response->getBody();

        $data = json_decode($body,true);
        return redirect('/clientapi/EditProfile87');
    }

    public function indexpassword87()
    {
        $client = new Client ();
        $Users = Auth::user()->name;

        return view('/apiclienttic/ubahpassword', ['name'=>$Users]);
    }

    public function ubahpassword87(Request $request)
    {
        $client = new Client();
        $Users = Auth::user();
        $response = $client ->request('POST', 'http://127.0.0.1/UAS_Backend/public/api/ubahpassword87/'.Auth::user()->id,
        [
            'form_params' => [
                'password' => $request->password,
                'repassword' => $request->repassword,
            ]
        ]);
        $data = User::all();
        $statusCode = $response->getStatusCode();
        $body = $response->getBody();


        if ($statusCode == 200) {
            return redirect('/clientapi/ubahpassword87');
        } else {
            return redirect('/clientapi/ubahpassword87')->with('errors', 'Password gagal diubah');
        }
    }
}