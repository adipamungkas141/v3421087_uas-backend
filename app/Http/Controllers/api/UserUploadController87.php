<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Resources\FormatApi;
use Illuminate\Support\Facades\Validator;

class UserUploadController87 extends Controller
{
    public function tambahKTP87(Request $request, $id)
    {
        $detail = User::find($id)->detail;

        if ($detail->foto_ktp != "foto_ktp.png") {
            if (file_exists(public_path('gambar/' . $detail->foto_ktp))) {
                unlink(public_path('gambar/' . $detail->foto_ktp));
            }
        }

        $file = $request->file('foto_ktp');
        $fileName = time() . '.' . $file->getClientOriginalExtension();
        $file->move(public_path('gambar/'), $fileName);

        $detail->foto_ktp = $fileName;
        $savePhoto = $detail->save();

        if ($savePhoto) {
            return new FormatApi(true, 'Berhasil upload ktp', null);
        } else {
            return new FormatApi(false, 'gagal upload', null);
        }
    }

    public function tambahPhoto87(Request $request, $id)
    {
        $detail = User::find($id);

        if ($detail->foto != "foto.png") {
            if (file_exists(public_path('gambar/' . $detail->foto))) {
                unlink(public_path('gambar/' . $detail->foto));
            }
        }

        $file = $request->file('foto');

        $fileName = time() . '.' . $file->getClientOriginalExtension();
        $file->move(public_path('gambar/'), $fileName);

        $detail->foto = $fileName;

        $savePhoto = $detail->save();
        if ($savePhoto) {
            return new FormatApi(true, 'Berhasil upload profil', null);
        } else {
            return new FormatApi(false, 'gagal upload', null);
        }
    }

    public function changeprofil87(Request $request, $id)
    {
        $detail = User::find($id);

        $validator = Validator::make($request->all (), [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'alamat' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'agama_id' => 'required',
        ]);

        if($validator->fails())
        {
            return new FormatApi(false, 'Validasi gagal', $validator->errors()->all());
        }

        $ubahprofil = User::find($id);

        $ubahprofil->update ([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'alamat' => $request->alamat,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'agama_id' => $request->agama_id,
        ]);
        if(!$ubahprofil)
        {
            return new FormatApi(false, 'Data profil tidak ditemukan', null);
        }

        return new FormatApi(true, 'Data profil berhasil menguabah data', null);
    }

    public function ubahpassword(Request $request, $id)
    {
        $user = User::find($id);

        if (!$user){
            return new FormatApi(false, 'User tidak ditemukan', null);
        }

        $validator = Validator::make($request->all(), [
            'password' => 'required|min:8',
            'repassword' => 'required|same:password',
        ]);

        if($validator->fails())
        {
            return new FormatApi(false, 'Validasi gagal', $validator->errors()->all());
        }

        $ubahpassword = User::find($id);
        $ubahpassword->update ([
            'password' => bcrypt($request->password),
        ]);
        
        if(!$ubahpassword)
        {
            return new FormatApi(false, 'Password tidak ditemukan', null);
        }

        return new FormatApi(true, 'Password berhasil menguabah data', null);
    }
    

}