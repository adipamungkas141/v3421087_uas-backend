<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\FormatApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;


class UsersApiController87 extends Controller
{
    public function index87()
    {
        $user = User::with('detail')->where('role', 'user')->get();
        return new FormatApi(true, 'Berhasil mendapatkan data user', $user);
    }

    public function showdetail87(Request $request, $id)
    {
        $user = User::with('detail')->find($id);
        if (!$user)
        {
            return redirect('/dashboard87')->with('error', 'User tidak ditemukan');
        }


        return new FormatApi(true, 'Berhasil mendapatkan data user', $user);

    }

    public function destroy87($id)
    {
        $user = User::find($id);
        if (!$user)
        {
            return new FormatApi(false, 'User tidak ditemukan', null);
        }

        $user->delete();
        return new FormatApi(true, 'Berhasil menghapus data user', null);
    }

    public function status87($id)
    {
        $user = User::find($id);
        

        if (!$user) {
            return new FormatApi(false, 'User tidak ditemukan', null);
        }

        $updateStatus = $user->update([
            'is_active' => $user->is_active == 1 ? 0 : 1
        ]);

        if ($updateStatus) {
            return new FormatApi(true, 'Berhasil mengubah status user', null);
        } else {
            return new FormatApi(false, 'Gagal mengubah status user', null);
        }
    }

    public function putDetailData87(Request $request, $id)
    {
        $user = User::with('detail')->find($id);
        if (!$user)
        {
            return redirect('/dashboard87')->with('error', 'User tidak ditemukan');
        }
    }

    public function putPhotoUser87(Request $request, $id)
    {
        $user = User::find($id)->with('detail');
        if(!$user)
        {
            return new FormatApi(false, 'User tidak ditentukan', null);
        }

        $validator = Validator::make($request->all(), [
            'photoProfil' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->fails())
        {
            return new FormatApi(false, 'Validasi gagal', $validator->errors()->all());
        }

        $imageName = time() . '.' . $request->photo->extension();
        $request->photo->move(public_path('images'), $imageName);

        $updatePhoto = $user->dateil->update([
            'foto' => $imageName,
        ]);

        if ($updatePhoto)
        {
            return new FormatApi(true, 'Berhasil mengubah foto user', null);
        }else{
            return new FormatApi(false, 'Gagal mengubah foto user', null);
        }

    }

    public function putPhotoKTP87(Request $request, $id)
    {
        $user = User::find($id)->with('detail');
        if(!$user)
        {
            return new FormatApi(false, 'User tidak ditentukan', null);
        }

        $validator = Validator::make($request->all(), [
            'photoKTP' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->fails())
        {
            return new FormatApi(false, 'Validasi gagal', $validator->errors()->all());
        }

        $imageName = time() . '.' . $request->photo->extension();
        $request->photo_ktp->move(public_path('images'), $imageName);

        $updatePhoto = $user->dateil->update([
            'foto_ktp' => $imageName,
        ]);

        if ($updatePhoto)
        {
            return new FormatApi(true, 'Berhasil mengubah foto KTP user', null);
        }else{
            return new FormatApi(false, 'Gagal mengubah foto KTP user', null);
        }   
    }

    public function putUserPassword87(Request $request, $id)
    {
        $user = User::find($id)->with('detail');

        if (!$user){
            return new FormatApi(false, 'User tidak ditemukan', null);
        }

        $validator = Validator::make($request->all(), [
            'old_password' => 'required|min:8',
            'password' => 'required|min:8',
            'password_confirmation' => 'required|same:password',
        ]);
    }

    public function putUserStatus87(Request $request, $id)
    {
        $user = User::with('detail')->find($id);

        if (!$user){
            return new FormatApi(false, 'User tidak ditemukan', null);
        }

        $validator = Validator::make($request->all(), [
            'is_active' => 'required|boolean',
        ]);

        if ($validator->fails())
        {
            return new FormatApi(false, 'validasi gagal', $validator->errors()->all());
        }

        $updateStatus = $user->update([
            'is_active' => $request->is_active,
        ]);

        if ($updateStatus)
        {
            return new FormatApi(true, 'Berhasil mengubah status user', null);
        }else {
            return new FormatApi(false, 'Gagal mengubah status user', null);
        }
    }
}
