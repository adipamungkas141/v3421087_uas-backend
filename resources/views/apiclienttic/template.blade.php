<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ADMIN | CRUD Agama</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ url('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ url('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ url('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ url('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ url('dist/css/adminlte.min.css') }}">
</head>
<body>
<div class="col-md-12">

    <!-- Profile Image -->
    <div class="card ">
        <div class="row">
            <div class="col-9">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Agama</h3>

                        <div class="card-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control float-right"
                                    placeholder="Search">

                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover text-nowrap">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama Agama</th>
                                    <th>Hapus</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                @foreach ($agama['data'] as $all)
                                <tr>
                                    <td>{{$all["id"]}}</td>    
                                    <td>{{$all["nama_agama"]}}</td> 
                                    <td style="width: 150px">
                                            <a href="{{ url ('/agama72/clientapi/editagama/'.$all['id']) }}"
                                                            class="btn btn-warning">Edit</a>
                                                            
                                                            <form action="{{ url('/agama72/clientapi/prosesdelete/'.$all['id']) }}" method="POST">
                                                                @csrf
                                                                @method('DELETE')<div class="card-footer">
                                                                    <button type="submit" class="btn btn-danger w-100">Hapus</button>
                                                                </div>
                                                            </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <div class="col-md-3">
    <div class="card">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Masukkan Agama Baru</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="{{ url('/agama72/clientapi/prosesaddagama') }}" method="POST">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="nama_agama">Nama Agama</label>
                        <input type="text" class="form-control" id="nama_agama" placeholder="Masukkan Agama"
                            name="nama_agama">
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-success w-100">Tambah Agama</button>
                </div>
            </form>
        </div>
    </div>
</div>
        </div>
    </div>
    <!-- /.card -->

</body>
</html>