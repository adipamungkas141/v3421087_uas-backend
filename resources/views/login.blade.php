<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>UTS Back End  | Login </title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="./plugins/fontawesome-free/css/all.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="./plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="./dist/css/adminlte.min.css">
</head>

<body class="hold-transition login-page" style="background: url('data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBYWFRgWFhYZGBgaHRwcHBocHBocGhoaHBwcGhwaIRocIS4lHB8rHxkcJjgmKy8xNTU1HCQ7QDs0PzA0NTEBDAwMBgYGEAYGEDEdFh0xMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMf/AABEIAJoBSAMBIgACEQEDEQH/xAAaAAADAQEBAQAAAAAAAAAAAAACAwQBAAUH/8QAMRAAAQMCBAUEAgMAAwADAAAAAQACESExA0FR8GFxgZGhErHB0QThIjLxE0LCFIKS/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/APp2I0vBinU95tr3CkbiQ9tT/IkU5c6mg5ARztw3QSOExnpO4SML8b0umSYoOsef4ivFBjmBtBQHiaRmO65jxMk8+VuisxGt9NRw3VTjAJrrSfmvQoNYSS4TpE8aZ++yTDQSanp2QswyHcK1ygjjewFf0nOwwLUqSdMpPsg21Qab3KaMSkpDd+FgoCCf1c30ogacfQg2WudMKTC4E7rsKljwg4gekREcuvutZ7ff6TgwLHgBBwEiqzEw5CXh4/qyK04tSDsaoEYuJCR66Vym4tYV6J+K+Nafr7Qf8TRWSIERNiTetcvHYOZj1jXLO3+Klhm6WzDuW0nO/HO9ZRtBBuT7Wg8skFAC2FgK2UEr2EVFY3Ck/MEExnWeIzpW0eOK9HGnJSfksJIgWF9JgHwggfhhzYnU562M8x2KD8bAh3StgaEkGBzM8Sr8P8apkn6/e4CP/wCMQb0iMr6209u4Y1lN+3ZExs1pOlwL/wC9Uxwzsiw2X13+0CXAAjXkd3ROxd/PhbikH1XoK0md1UjDIjW3fM51M9UF8gxNCjeaGL/KlaKdI8p2G6KFBzRBjVM9FFwxBFUbyIqgQ6lUTNd9ELgHAHLjnNM0RFN6IF4zjbukYn6nlN1RhYWvv5Rz/qDzpmT8HqePb6SQfU6oEg/xoaXnmYPm9VdiPGnfT/ED8NwJIBIiOMjKp37hL6PS12Y04Uy1+wqnOrIrx5aIXstRppUajITqBCItbE3ER3gd54zUBA/1DmsSMJwI40N8poD0osQV/wDGJnpfr8BSAQ4En+U94EZ5QZ85FUlkkTUe1Mt3TfQKSLc9lAMB0UkX30omBA85DdKLHvoYqafXdAb2Aitt/SEOkkZCN+yn9ZBkERWeyeHg2QLNM92lBhEeo6H2nLeSNogSYmppOtKImODqio1/1BPhtu02Ekd85RRW9InpWvj3Thh3jj+1rGQ7332QUNKT+S6n+/CfdIxmm42NUAemB4Pn7UuLUzIA46CK95001VOLiCCNOftnZeZiSTFhJoDmAYoKG3jsHqYcEQIFMtDMH5WjBHXmTzU+CwQOtY5fQ8KhhOuxkg3Dw44Dgu9Vd+y08SsJQMa+iIIWQmIAxXUolNrlCa8UhCwQIMbqg4NQueCY3uicLLggUGLjhxY7PvdOXFBD+S0UMboklorziPf6VmM0OEZ+VM/Bl1zEzGRyFuqDmYkC44+6o9SQ6GzFjG+Ga3CfQUgGxtkgYPryiw5OZpuEHq46JmDcxvcIOdvzVFgtjkjLFzRWSg4peNUHfhbiPy5IGYs5QgEYcjmuggyePhMZGR9juyxjK3QA7BaQRl19pp+kHppFgO1t1THDOoNupjoUcaIEswGgmlTeeNSuTHXXINJuJE7inRZ6+NuBHjNa4De+C44YpPbJADnUkeM8v0lufTMWt1TXip3lCU4G5183QR4uO6RS5EnTUkZWNeHGicH1SKkOBkilRpAI1Cte3L5jOZ9vdKeIgi2YnKmvDyEBl5PpJoP+3MxbX9Kj8Z5iuX0PKz8dlK5kzOaexgyQc58SdKoGvJFvIsg/IIIMEZHhqk/jyHgE1vE3/ifNKoPQZRLxXxzXB+5S/wAh1tTXt/vlAprss8teFda+FrPxhGsVjjJS2Yonj7DSp50+1UDxQLIjymtvaLLgZvz7ImjcoNK70SiDUbUHMaAtBWExVTOxCLeeW/CBvqExndCBBvetd6ICRHqF+NqlbhQ6TnzQMc42CMFC5w1rvfVKa8AkSTOWl0FLkGIaJTsXS+99V1aIMw65V+wtLN8SufbTf6WscT+tUGBo3RJbUy4ERQeCTGQnNUFwspseQCReLZ9O/hAWIPRUToZ06cQE9msVzUn45dmczHLIE6qoFAbnrvUsDVzTBAy+UAGTWDWFxZqnuctKCJxIM7Nd908OETYLnNBQsiLU3kgx70wAAb5rmt0QveREds0BOC5YIityuQTMdBTsPFkxn9b90p2GZneoy5JuAPooHOwwaoXtFuwRh0rS1BLi4OY7Zbp5QtwJgEW13wVhC2UErwRkTyWg/wDXunPYcu6FjNUC34cbHdQ4+GbyYHEVvw+V6eIKKXGoN7FkE7cL+LYLqZAxa87/AEv80kiAYNK1rfKK8uC4Ph5i0VvqIjoTYrgCP5GsA0vU1A4GnvRAGFgOqTAnmcjU16a34QeHjRc5k1gc5/zLJB+T6xDmEzNsjDZtkCYtBpzRfjATLmyY9Ji0AXigzyQWMYRBmY0VWG1LwcQOqLb+VTKDihcYRJeNMU4cPOQQDiVHHJIL7is8sjO+iYD6RU8zZJw8YOmBB6VFYNDZAWCJbDh0jedUz1RAGf6+yuaxHlWyBDprrv5S2DxTNPxRUR+t1KB83G+m/sCJz3v7Wl2m6pHqmOkjqU9rNa++6oAaZ3x/aYwSD/mvZC8QnRFq9UCOG6b8JT3VAImaTz+fpUYjvjhlP2uYyalBP+LSmmxSL50VQBMZdtBX3QMAkjTyE5hrEIDEJX5IpROKDEBKBDCjBSvS4Tbvx5W+0p5IIN61jTWL0hBSDJRtEhIY+bcPMKnDQdhj57dEvGEHmnJX5J30QLe+i5A3Te5XICbiDfLyjcYHNTMZ6TM5QBQaRHhOxH0Byz4U/aBzG73knFTNfRaMT4QPKwIWYgKNBxWBa1dCBb3COimxQQZHXym4gg80OIZFUHm4uKQ70nOQDYSS2OM6dOutaJaCdc6UIj288V35LHGCNbaj06m1TfZJoDs4M9xaBw/aCh7BfPd76IQypAyOxwGfPzrMT+MH+wmQNQBrnEaIGtImbj99skFX449lUCp8IQqPUEGoMR9ESndfVBP+SCRF7fNDwskswagijqaWmI/XGqtLZ3zha1vv8+EAseaTHSiMPJoKVWEDTcrDkcyJ4HKfZBrLGT/lI8pbzTpmiDqxFxVMdh6U3zQR4jYNrk11Ez9Ktue8gg/IbQAa+yNopFNwgxzCT58on4mlfZYz4zz+Ugv169yEDZk8zw09k5rQErAi+um81zsQa7og5wgzvT5RtH8pBpsXUn5DySIoPORnhoiL7zP3w90FrCen+rXuSWOgSc9jwlvd6hWm/r3QL/LdXOOBsMz59rKYs9YbN84FCDEi5Tr5Z3+ETMP+s04RagEQgd+MeSoa5JwxG+Kz1Q4xnH+b1QUJL3iJ3omNWYkdM0EriAaZ9u3Rcjx8MHON6LkEDsV4rEg/QBHG+mRTn4jSATYkVHfsjwsAuFSQJJ8RF6aoMf8AGECn9Yi6DhjtENIrSJ1+VQz0urkk+igNCIjXd0sH0uFImkXMnKtdjRB6AcjYTnn4UwNe6VhYrjAmDNY8U6RdBeiCW7FC1j5CDsSo1UpP7VhUmK2D6svpAprTT51/wpWMIoOHfYKew0rT9bCTiM9R/jSIryNd/SDnGR6gCSYtBgGhIjLPdHMEwd7upy0PcIo0XuJPLgfdVsEU4++7IDc7fwsY+R/mXstcJSmYcUvfqdSgcx3qBk1NOSANihhFh4ZadQeG9U5AIZnkgJWsfTfVZiCUGONPC70g3mQP0ltkyBSN9FrgR396Ui1/CBhIFta/5nktw6uvOk8d7qhaw525o2MAQEWeEtuGQansqCUD2AlAvENKU7fPBTMwqmtM5A3vmqy3JA9v8SBnMcygB4/iagCL5RxmkQEtgzjlrpHjwgDyKe/HXTmmYZrGSDnwNxvfRWHOcz7cd5QnPbIgR7pP5rTBIvl8+PhAxmP6jERzy8rBiVA61plvyp2u5zyNBArWuSxwItXqb8+56IHOjr+tEXqJtcVBmY1zrc90v8YTXX6qrGs3vmgBriK+IyTH1jnPBLifve7pgbUQLU+vnugc1q51iha/gl4+JAjYQJez9Z1yXLcR0jTcLkBtxmxam+6zEqRFR+lPhsOtIzHAnWsJvp0zr15IHYPmUH/x6yTIGXPzYLMTBOVc610sqYp2QQ/lsBEGoz7a91mH/FwtHaJEeTRPxWcaefspJdW2l9Yn4HhAT6OPjnb4nqnsOytbDhPwhw2ECqAw/uue2QRvh7IH7yWvdvugUcM2nn0j9rntiBEUpGgrCBkhxisUi2kSUx4m+/lBxbAK1reuSxsmpgDK9FSxgCAfQmelB6zMIX4qBspeK6ixrayc0jExCcqC/bXkgay2lEpz95rC4wJzoKnieiVjCkDryrI4IDY+lBMmLxkm4L5EO1pbKNFBgv8A+1Mjr/ETnmZhUev+NBbS9vr3QWPqEDXzAFteUIPVLDqL0OXC+80r8Z05GlB3j3mqCxwlcxhBqsYUzEcg1zUt9KrG4m/CY9tCg8/FMmRw/wDzdbhsJHqNJrrlvdixqi3v34qXExC2LxNRcER1OeV4HJBcWmAGmK18aIDgSIJORvxnophjH1ekaNisSSR4t3zVOH/cTUmfufbv0QE8EWHWMhYUPyp2YJ/7VNzT96eyqIrneetfj7RenNAljI3fjx/xUALHNXerfZBjR/I9CN9V3rhxpmewCQHncLXukNJHHjMW3oga55SfXJp/nHjZL/nkATFgaz1pnXlmtDQ2kiYJ85aCyBpe0SCST7TFJFlym/4zz+pFZ73vHbkFbMOtt892T24dkTWRbNEgBzd76Jb8QDx8/SY58qd/9ul8hE/aBeJ+QcoNe9ax03dbgYnqaDacj7+PKF7RwHHTcoWGIBqMjmBTugqwz23CP1RW6ThMIEGtT17osNtBNd/tBrmgnvQ9ljx3WPJuOH+I3Opz79kE+E+STMiTB6De4TSyUoHflOw5NtdhBgFhs/pPBQuZG7IMfEgUvv5QG46oXEX7JOC2ZM00ncftHig3GW7b+w5+Icqc97lJxsSs+kZZ9Zst9c9986BY9gO+nSqBxaHgQYz8GiRjYTgRXMWpSbDjdOw2QdLfpGWy0A3EdUETS2kSNRcZV1MeeyNr6d7kcr5rcbBior0k3H1oluZSRwpW3+mmygFmIQawBHz4vsVVLcIgk9ae2/0py2vfXX9HZVWE06xPTrv6QPw1jnX5/C0Ok8lpjWeOvZAp0x8dCta+QR57rHtBEEc9eNlEHkG9c63j3vmgra0mkQN5b6rmMgAafKBjjrl0mc+mwnsk990QTvw2gl0QYqdAK9aapbMWDJJi0aWHvXurHYYIIIn5U7PxW1/7GtTB0NZmsidEB4r6zBreMoP1otGJO98FnoyNfn6/aEYQ17maaIGueN899kgO0pw7acUOIxwtHzSvvySzhEO/scuV+U2nNBScNrrSDUcenqvmjYTmayd+UnAzn/ePndCqGgGu9/aCbDwzMm8VIpWmWlOKZ/xVJ457pmnBqMYewgSWZbsFyOADvkuQE8yeAQh0VRvnpml+icqbn2jqg4G+60otkc92WPeGjQLQys1qgVjYdKE0rx5pDnyDnSudxHY2VTn1gDmdPsrDgNzF/HEd0DRaqWKHnXzzSsIemPVW2prAGnA+EWKZI4WOqAnT44LsIggDSBvVLDyZPWnk+VmG3+R3wHzuEDCw3y45dunhNDSAAOS4LDkCKfIqLIGuE0Ujm8O3tQSqXFTYkgxrbfMoDwnAinbRETCzCbUnl8rcVtPGqCUNMk+2pP7CbMGDvcLIkaXrmkYgEgVNhSR/sacUFXqpy+OG7LnjfsswsOhqa8tI05rThmDWfEICJkA9R2/aWXSe1/Fr3CZhMIBHLvsKcOjwNNEGsbrJua8rdkwuhYG8Nn9wiOGJia74ICDkTHC24U7mtoC4jsPMZo/RmD81mUDHlRmrgQOnGSTWM038lslp597x4XMEadKAER2+kGsFN9lVgNoh/GZATkAPw51U7muFlWSlvQS/lPht4mlePOmqUx02104VHmO6fiAGhE9OdN6pOKaNG4EDKdyg0Macu/kLMLDPqJggVGYrTJb+PU+qNRxFfiqoKCckAkaxn28hUYMoLUiU5grTzdA1q0LoXOKDozXIA/ouQY80WDghN+6zD/s7ogF7xJnLlpeEl0/1MxAinOnMRKH8mx5n2TPyP6jmPZyDMN8+qBp/t04v38KY3HI+wRHLn8BAz8j+s5yCOc31zROxP4gxpQ8flK/I/qOfyhfZnL/yUCmNMxNYMcMppNPGVVScMyQDAOmV/NfHdP439/8A6H3VpugDEeQcoOdZ6jf2YfI377sgxv7Dog/Hz6e5QMc8ASVzcMmqW/8Av/8AVVi3RALGQmBYN91qCZ8ShOGJlZj37+zkbP6dCgleCTE6CBlkagI2PIrJInPTWVv2flEf671QUsqJQegX490eFbusxvn4KAGkC0bukuk3tbnXTkUTP7dflyz9oFOgDKnbS3zzWPxPT6okVPKw1C7H/r0/9JX5N+v/AJYgpLwRBvFoI8HinYTK75b6JL7DmFTh77lA1cl5BGckGOMKf8h7gaW52JIv28p7s+Q91N+TYc0Gt/lXLTqfrdkvHaCcuZ0oQPCdi59VK2zOR9kD8OlK33uExxpQ/SFuW8wiFhzQZh/SNh77+0tn18om3QZ/yEzW3+/W780E3KSy45H/AMp+nI+5QO9IXLMLPouQf//Z');">
    <div class="login-box">
        <!-- /.login-logo -->
        <div class="card card-outline card-primary">
            <center>
                <a href="./index2.html" class="h4"><b>System Profile</b></a>
            </center>
            <div class="card-header text-center">
                <img src="http://spesialis1.orthopaedi.fk.unair.ac.id/wp-content/uploads/2021/02/depositphotos_39258143-stock-illustration-businessman-avatar-profile-picture.jpg" width="125px" style="position:center;" style="display: block; margin-left:auto; margin-right:auto;">
            </div>
            <div class="card-body">
                <p class="login-box-msg"><b>Login</b></p>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                <form action="{{ url('87signin') }}" method="POST">
                    @csrf

                    <div class="input-group mb-3">
                        <input type="email" name="email" class="form-control" placeholder="Email">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" name="password" class="form-control" placeholder="Password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8">
                        </div>
                        <!-- /.col -->
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block">Login</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>

                <a href="{{ url('/signup87') }}" class="text-center">Crate account</a>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        <br>
        <div class="login-box">
            <div class="card card-outline card-primary">
            <div class="card-body">
                <h4>Keterangan :</h4>
                <p class="login-box"><b>Bisa menggunakan admin baru yang dibuat melalui register atau menggunakan akun admin yang sudah ada</b></p>
                <p class="login-box"><b>Login Admin</b></p>
                <p class="login-box"><b>Username : admin@admin.com</b></p>
                <p class="login-box"><b>Password : 12345678</b></p>
            </div>
            </div>
        </div>        
    </div>

    <!-- /.login-box -->

    <!-- jQuery -->
    <script src="./plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="./plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="./dist/js/adminlte.min.js"></script>
</body>

</html>
