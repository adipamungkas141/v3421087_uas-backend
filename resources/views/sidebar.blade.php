<ul>
    @if (Auth::check() && Auth::user()->role == 'admin')
        <div>
            <a href="{{ url('dashboard87') }}" class="nav-link">
                <p>
                    Dashboard
                </p>
            </a>
        </div>
        <div>
            <a href="{{ url('/user/clientapi/userlist') }}" class="nav-link">
                <p>
                    Dashboard Api
                </p>
            </a>
        </div>
        <div>
            <a href="{{ url('agama87') }}" class="nav-link">
                <p>
                    Agama
                </p>
            </a>
        </div>
        <div>
            <a href="{{ url('/agama87/clientapi/listagama87') }}" class="nav-link">
                <p>
                    Agama Api
                </p>
            </a>
        </div>
        <div>
            <a href="{{ url('logout87') }}" class="nav-link">
                <p>
                    Logout
                </p>
            </a>
        </div>
        <div>
            <a href="{{ url('logout87') }}" class="nav-link">
                <p>
                    Logout Api
                </p>
            </a>
        </div>
    @else
    
        <div>
            <a href="{{ url('welcome87') }}" class="nav-link">
                <p>
                    welcome
                </p>
            </a>
        </div>
        <div>
            <a href="{{ url('/clientapi/UserProfile87') }}" class="nav-link">
                <p>
                    welcome Api
                </p>
            </a>
        </div>
        <div>
            <a href="{{ url('profile87') }}" class="nav-link">
                <p>
                    Dashboard
                </p>
            </a>
        <div>
        <div>
            <a href="{{ url('/clientapi/editprofile87') }}" class="nav-link">
                <p>
                    Dashboard Api
                </p>
            </a>
        </div>
        <div>
            <a href="{{ url('/changePassword87') }}" class="nav-link">
                <p>
                    Ganti Password
                </p>
            </a>
        <div>
        <div>
            <a href="{{ url('/clientapi/ubahpassword87') }}" class="nav-link">
                <p>
                    Ganti Password Api
                </p>
            </a>
        <div>
        <div>
            <a href="{{ url('logout87') }}" class="nav-link">
                <p>
                    Logout
                </p>
            </a>
        </div>
    @endif
</ul>
