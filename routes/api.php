<?php

use App\Http\Controllers\api\ControllerApiAgama87;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\UsersControllerApi87;
use App\Http\Controllers\api\DetailDataApiController87;
use App\Http\Controllers\api\UserUploadController87;
use App\Http\Controllers\api\UsersApiController87;  



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Api
// Route::get('/agama87/listagama87', [ControllerApi::class, 'index']);
// Route::get('/agama87/detailtagama87/{id}', [ControllerApi::class, 'show']);
// Route::post('/agama87/tambah87', [ControllerApi::class, 'store']);
// Route::put('/agama87/update87/{id}', [ControllerApi::class, 'update']);
// Route::get('/agama87/delete87/{id}', [ControllerApi::class, 'destroy']);

// Route::get('/agama87/listagama', [UsersControllerApi87::class, 'getAgama87']);
// Route::post('/agama87/add', [UsersControllerApi87::class, 'postAgama87']);
// Route::get('/agama87/detailagama/{id}', [UsersControllerApi87::class, 'getDetailAgama87']);
// Route::put('/agama/{id}', [UsersControllerApi87::class, 'putAgama87']);
// Route::delete('/agama/{id}', [UsersControllerApi87::class, 'deleteAgama87']);s

//Agama
Route::get('/agama87/listagama87', [ControllerApiAgama87::class, 'index87']);
Route::post('/agama87/tambahagama87', [ControllerApiAgama87::class, 'store87']);
Route::get('/agama87/detailtagama87/{id}', [ControllerApiAgama87::class, 'show87']);
Route::put('/agama87/update87/{id}', [ControllerApiAgama87::class, 'update87']);
Route::delete('/agama87/{id}', [ControllerApiAgama87::class, 'destroy87']);

//Admin Utama
Route::get('/detail87', [DetailDataApiController87::class, 'index87']);
Route::post('/detail87/create', [DetailDataApiController87::class, 'store87']);
Route::put('/detail87/update/{id}', [DetailDataApiController87::class, 'update87']);
Route::delete('/detail87', [DetailDataApiController87::class, 'destroy87']);

//User
Route::post('/uploadApiPhotoProfil87/{id}', [UserUploadController87::class, 'tambahPhoto87']);
Route::post('/uploadApiPhotoKTP87/{id}', [UserUploadController87::class, 'tambahKTP87']);
Route::post('/uploadApiChangeProfil/{id}', [UserUploadController87::class, 'changeprofil87']);
Route::post('/ubahpassword/{id}', [UserUploadController87::class, 'ubahpassword']);

Route::get("/users87", [UsersApiController87::class, "index87"]);
Route::post("/users87/{id}", [UsersApiController87::class, "store87"]);
Route::get("/users87/{id}/detail", [UsersApiController87::class, "showdetail87"]);
Route::put("/users87/{id}", [UsersApiController87::class, "update87"]);
Route::delete("/users87/{id}", [UsersApiController87::class, "destroy87"]);
Route::put("/users87/status87/{id}", [UsersApiController87::class, "status87"]);

// Route::put('/users', [UsersControllerApi87::class, 'getUser87']);
// Route::put('/users/{id}', [UsersControllerApi87::class, 'getDetailUser87']);
// Route::put('/users/{id}', [UsersControllerApi87::class, 'putDetailUser87']);
// Route::put('/users/{id}/status', [UsersControllerApi87::class, 'putUserStatus87']);
// Route::put('/users/{id}/password', [UsersControllerApi87::class, 'putUserPassword87']);
// Route::put('/users/{id}/photo', [UsersControllerApi87::class, 'putUserPhoto87']);
// Route::put('/users/{id}/ktp', [UsersControllerApi87::class, 'puttKTP87']);