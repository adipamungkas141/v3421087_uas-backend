<?php

use App\Http\Controllers\Admin87Controller;
use App\Http\Controllers\Login87Controller;
use App\Http\Controllers\Users87Controller;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiClientAgamaController87;    
use App\Http\Controllers\api\ApiClientUsers87;
use App\Http\Controllers\api\UserProfilController87;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/signup87');
});

Route::group(['middleware' => ['initakLogged']], function () {
    // Login & Register
    Route::view('/signup87', 'register');
    Route::view('/87signin', 'login');
    Route::post('/signup87', [Login87Controller::class, 'registerHandler87']);
    Route::post('/87signin', [Login87Controller::class, 'loginHandler87']);
});

Route::group(['middleware' => ['iniAdmin']], function () {
    //dashboard && detail user
    Route::get('/dashboard87', [Admin87Controller::class, 'admindashboardpaged87']);
    Route::get('/detail87/{id}', [Admin87Controller::class, 'detailAdminPage87']);

    // update user
    Route::get('/update87/user/{id}/status', [Admin87Controller::class, 'updateStatusUser87']);
    Route::post('/update87/user/{id}/agama', [Admin87Controller::class, 'updateAdminAgamaUser87']);

    // CRUD AGAMA
    // Show all agama
    Route::get("/agama87", [Admin87Controller::class, "adminagamapage87"]);
    // add agama
    Route::post("/agama87", [Admin87Controller::class, "admincreateAdmin87"]);
    // show edit agama & update agama
    Route::get("/agama87/{id}/edit", [Admin87Controller::class, 'editAdminAgamaPage87']);
    Route::post("/agama87/{id}/update", [Admin87Controller::class, 'ubahAdminAgama87']);
    // delete agama
    Route::get("/agama87/{id}/delete", [Admin87Controller::class, 'hapusAdminAgama87']);

    //ApiAgamaClient
    Route::get('/agama87/clientapi/listagama87', [ApiClientAgamaController87::class, 'index87']);
    Route::get('/agama87/clientapi/tampilanagama87', [ApiClientAgamaController87::class, 'create87']);
    Route::get('/agama87/clientapi/editagama/{id}', [ApiClientAgamaController87::class, 'show87']);
    Route::post('/agama87/clientapi/prosesaddagama', [ApiClientAgamaController87::class, 'store87']);
    Route::put("/agama87/clientapi/proseseditagama/{id}", [ApiClientAgamaController87::class, 'update87']);
    Route::delete("/agama87/clientapi/prosesdelete/{id}", [ApiClientAgamaController87::class, 'destroy87']);

    //Admin
    Route::get('/user/clientapi/userlist', [ApiClientUsers87::class, 'index87']);
    Route::get('/clientapi/detail87/{id}', [ApiClientUsers87::class, 'show87']);
    Route::get('/clientapi/delete87/{id}', [ApiClientUsers87::class, 'destroy87']);
    Route::get('/clientapi/status87/{id}', [ApiClientUsers87::class, 'status87']);
    Route::post('/clientapi/update87/agama/{id}', [ApiClientUsers87::class, 'update87']);


});

Route::group(['middleware' => ['iniUser']], function () {
    // dashboard user
    Route::get('/profile87', [Users87Controller::class, 'halamanProfil87']);
    Route::get('/welcome87', [Users87Controller::class, 'welcome']);
    //change Password
    Route::get('/changePassword87', [Users87Controller::class, 'editPasswordPage87']);
    Route::post('/updatePassword87', [Users87Controller::class, 'ubahUserPassword87']);

    // edit profile user
    Route::post('/perbaruiProfil87', [Users87Controller::class, 'updateProfil87']);
    Route::post('/unggahPicProfil87', [Users87Controller::class, 'unggahFotoProfil87']);
    Route::post('/unggahPicKTP87', [Users87Controller::class, 'unggahFotoKTP87']);

    //User
    Route::get('/clientapi/UserProfile87', [UserProfilController87::class, 'index87']);
    Route::get('/clientapi/editprofile87', [UserProfilController87::class, 'edit87']);
    Route::post('/clientapi/uploadktp87', [UserProfilController87::class, 'uploadktp87']);
    Route::post('/clientapi/uploadphoto87', [UserProfilController87::class, 'uploadphoto87']);
    Route::put('/clientapi/changeprofil87', [UserProfilController87::class, 'changeprofil87']);

    //ubah passsword
    Route::get('/clientapi/ubahpassword87', [UserProfilController87::class, 'indexpassword87']);
    Route::post('/clientapi/ubahpassword87', [UserProfilController87::class, 'ubahpassword87']);

});

Route::get('/logout87', [Login87Controller::class, 'logoutHandler87'])->middleware('iniLogged');


